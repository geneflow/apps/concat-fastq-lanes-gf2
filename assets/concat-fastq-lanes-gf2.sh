#!/bin/bash

# Concatenate lanes for a standard Illumina sample wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input_folder => Input Folder"
    echo "  --prefix => Sample Prefix"
    echo "  --extension => FASTQ Extension"
    echo "  --sample_name => Sample Name"
    echo "  --output => Output Folder"
    echo "  --exec_method => Execution method (environment, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input_folder:,prefix:,extension:,sample_name:,output:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
EXTENSION="fastq"
EXEC_METHOD="auto"
EXEC_INIT=":"
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input_folder)
            if [ -z "${input_folder}" ]; then
                INPUT_FOLDER=$2
            else
                INPUT_FOLDER="${input_folder}"
            fi
            shift 2
            ;;
        --prefix)
            if [ -z "${prefix}" ]; then
                PREFIX=$2
            else
                PREFIX="${prefix}"
            fi
            shift 2
            ;;
        --extension)
            if [ -z "${extension}" ]; then
                EXTENSION=$2
            else
                EXTENSION="${extension}"
            fi
            shift 2
            ;;
        --sample_name)
            if [ -z "${sample_name}" ]; then
                SAMPLE_NAME=$2
            else
                SAMPLE_NAME="${sample_name}"
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT="${output}"
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD="${exec_method}"
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT="${exec_init}"
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input_folder: ${INPUT_FOLDER}"
echo "Prefix: ${PREFIX}"
echo "Extension: ${EXTENSION}"
echo "Sample_name: ${SAMPLE_NAME}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT_FOLDER input

if [ -z "${INPUT_FOLDER}" ]; then
    echo "Input Folder required"
    echo
    usage
    exit 1
fi
# make sure INPUT_FOLDER is staged
count=0
while [ ! -d "${INPUT_FOLDER}" ]
do
    echo "${INPUT_FOLDER} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${INPUT_FOLDER}" ]; then
    echo "Input Folder not found: ${INPUT_FOLDER}"
    exit 1
fi
INPUT_FOLDER_FULL=$(readlink -f "${INPUT_FOLDER}")
INPUT_FOLDER_DIR=$(dirname "${INPUT_FOLDER_FULL}")
INPUT_FOLDER_BASE=$(basename "${INPUT_FOLDER_FULL}")



# PREFIX parameter
if [ -n "${PREFIX}" ]; then
    :
else
    :
    echo "Sample Prefix required"
    echo
    usage
    exit 1
fi


# EXTENSION parameter
if [ -n "${EXTENSION}" ]; then
    :
else
    :
fi


# SAMPLE_NAME parameter
if [ -n "${SAMPLE_NAME}" ]; then
    :
else
    :
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Folder required"
    echo
    usage
    exit 1
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="environment auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v cat >/dev/null 2>&1; then
        AUTO_EXEC=environment
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    environment)
        MNT=""; ARG=""; CMD0="SAMPLE=`ls "${INPUT_FOLDER_FULL}" | grep "^${PREFIX}" | grep 'L001_R1' | head -n 1 | sed 's/\(.*\)_L001_R1.*\.\(fastq\|fq\)\(\|\.gz\)$/\1/g'` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        MNT=""; ARG=""; CMD0="READFMT=R ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        MNT=""; ARG=""; CMD0="EXT=`ls "${INPUT_FOLDER_FULL}" | grep "^${PREFIX}" | grep 'L001_R1' | head -n 1 | sed 's/.*_L001_R1.*\.\(fastq\|fq\)\(\|\.gz\)$/\1/g'` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        MNT=""; ARG=""; CMD0="ZIP=`ls "${INPUT_FOLDER_FULL}" | grep "^${PREFIX}" | grep 'L001_R1' | head -n 1 | sed 's/.*_L001_R1.*\.\(fastq\|fq\)\(\|\.gz\)$/\2/g'` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        if { [ -z "${EXT}" ] || [ -z "${SAMPLE}" ]; }; then MNT=""; ARG=""; CMD0="SAMPLE=`ls "${INPUT_FOLDER_FULL}" | grep "^${PREFIX}" | grep '_R1\|_1' | head -n 1 | sed 's/\(.*\)_\(R\|\)1.*\.\(fastq\|fq\)\(\|\.gz\)$/\1/g'` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; MNT=""; ARG=""; CMD0="READFMT=`ls "${INPUT_FOLDER_FULL}" | grep "^${PREFIX}" | grep '_R1\|_1' | head -n 1 | sed 's/.*_\(R\|\)1.*\.\(fastq\|fq\)\(\|\.gz\)$/\1/g'` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; MNT=""; ARG=""; CMD0="EXT=`ls "${INPUT_FOLDER_FULL}" | grep "^${PREFIX}" | grep '_R1\|_1' | head -n 1 | sed 's/.*_\(R\|\)1.*\.\(fastq\|fq\)\(\|\.gz\)$/\2/g'` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; MNT=""; ARG=""; CMD0="ZIP=`ls "${INPUT_FOLDER_FULL}" | grep "^${PREFIX}" | grep '_R1\|_1' | head -n 1 | sed 's/.*_\(R\|\)1.*\.\(fastq\|fq\)\(\|\.gz\)$/\3/g'` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; if { [ -z "${EXT}" ] || [ -z "${SAMPLE}" ]; }; then MNT=""; ARG=""; CMD0="fail 'Valid file format not detected in input folder' ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; MNT=""; ARG=""; CMD0="LANES=no ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; else MNT=""; ARG=""; CMD0="LANES=yes ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        MNT=""; ARG=""; CMD0="echo "LANES=${LANES}" ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        if [ -z "${SAMPLE_NAME}" ]; then MNT=""; ARG=""; CMD0="SAMPLE_NAME=${SAMPLE} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ "${LANES}" = "yes" ]; then MNT=""; ARG=""; CMD0="COUNT1=`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_L..._${READFMT}1.*\.${EXT}${ZIP}$" | wc -l` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; MNT=""; ARG=""; CMD0="COUNT2=`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_L..._${READFMT}2.*\.${EXT}${ZIP}$" | wc -l` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; else MNT=""; ARG=""; CMD0="COUNT1=`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_${READFMT}1.*\.${EXT}${ZIP}$" | wc -l` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; MNT=""; ARG=""; CMD0="COUNT2=`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_${READFMT}2.*\.${EXT}${ZIP}$" | wc -l` ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ "${COUNT1}" -eq "0" ]; then MNT=""; ARG=""; CMD0="fail 'No lanes detected for R1' ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ "${COUNT2}" -gt "0" ] && [ "${COUNT1}" -ne "${COUNT2}" ]; then MNT=""; ARG=""; CMD0="fail 'Number of lanes for R1 does not match number of lanes for R2' ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ "${LANES}" = "yes" ]; then MNT=""; ARG=""; CMD0="LIST1=(`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_L..._${READFMT}1.*\.${EXT}${ZIP}$" | sort`) ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; else MNT=""; ARG=""; CMD0="LIST1=(`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_${READFMT}1.*\.${EXT}${ZIP}$" | sort`) ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ -n "${ZIP}" ]; then MNT=""; ARG=""; CMD0="cat ${LIST1[@]} > ${OUTPUT_FULL}/${SAMPLE_NAME}_R1.${EXTENSION}${ZIP} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ -z "${ZIP}" ]; then MNT=""; ARG=""; CMD0="cat ${LIST1[@]} | gzip > ${OUTPUT_FULL}/${SAMPLE_NAME}_R1.${EXTENSION}.gz ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ "${LANES}" = "yes" ]; then MNT=""; ARG=""; CMD0="LIST2=(`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_L..._${READFMT}2.*\.${EXT}${ZIP}$" | sort`) ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; else MNT=""; ARG=""; CMD0="LIST2=(`ls -d ${INPUT_FOLDER_FULL}/* | grep "/${SAMPLE}_${READFMT}2.*\.${EXT}${ZIP}$" | sort`) ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ "${COUNT2}" -gt "0" ] && [ -n "${ZIP}" ]; then MNT=""; ARG=""; CMD0="cat ${LIST2[@]} > ${OUTPUT_FULL}/${SAMPLE_NAME}_R2.${EXTENSION}${ZIP} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        if [ "${COUNT2}" -gt "0" ] && [ -z "${ZIP}" ]; then MNT=""; ARG=""; CMD0="cat ${LIST2[@]} | gzip > ${OUTPUT_FULL}/${SAMPLE_NAME}_R2.${EXTENSION}.gz ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; fi; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

